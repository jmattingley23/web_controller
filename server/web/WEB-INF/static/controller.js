var joinUrl = "/joinRoom";
var wsUri = "wss://jmatt.xyz:";
var websocket;

function getPort() {
    roomId = document.getElementById("roomIdField").value;
    $.ajax({
        url: joinUrl,
        type: "GET",
        headers: {"roomId": roomId},
        success: function(data) {
            openWebSocket(data);
        }
    });
}

function openWebSocket(port) {
    websocket = new WebSocket(wsUri + port);
    websocket.onopen = function(evt) { onOpen(evt) };
    websocket.onclose = function(evt) { onClose(evt) };
    websocket.onmessage = function(evt) { onMessage(evt) };
    websocket.onerror = function(evt) { onError(evt) };
}

function closeWebSocket() {
    websocket.close();
}

function onOpen(evt) {
    writeToScreen("CONNECTED");
}

function onClose(evt) {
    writeToScreen("DISCONNECTED");
}

function onMessage(evt) {
    writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
}

function onError(evt) {
    writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function doSend() {
    writeToScreen("SENT: " + "test");
    websocket.send("test");
}

function writeToScreen(message) {
    output = document.getElementById("output");
    var pre = document.createElement("p");
    pre.style.wordWrap = "break-word";
    pre.innerHTML = message;
    output.appendChild(pre);
}