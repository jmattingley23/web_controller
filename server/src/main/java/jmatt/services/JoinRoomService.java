package jmatt.services;

import jmatt.data.RoomSingleton;

public class JoinRoomService {
    public static int joinRoom(String roomId) {
        int socket = RoomSingleton.getInstance().getSocketForRoomId(roomId);
        RoomSingleton.getInstance().useRoom(roomId);
        return socket;
    }
}
