package jmatt.services;

import jmatt.data.RoomSingleton;

public class CreateRoomService {
    public static String createNewRoom() {
        return RoomSingleton.getInstance().createRoom();
    }
}
