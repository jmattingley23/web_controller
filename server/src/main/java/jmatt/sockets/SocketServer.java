package jmatt.sockets;

import jmatt.secure.Keys;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.DefaultSSLWebSocketServerFactory;
import org.java_websocket.server.WebSocketServer;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.*;
import java.security.cert.CertificateException;

public class SocketServer extends WebSocketServer {
    private int port = -1;
    private WebSocket roomSocket;
    private boolean roomJoined = false;


    public SocketServer(int port) {
        super(new InetSocketAddress(port));
        this.port = port;

        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            FileInputStream fileInputStream = new FileInputStream("/home/ubuntu/keystore");
            ks.load(fileInputStream, Keys.KEYSTORE_PASSWORD.toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(ks, Keys.KEYSTORE_PASSWORD.toCharArray());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            this.setWebSocketFactory(new DefaultSSLWebSocketServerFactory(sslContext));
            this.start();

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | KeyManagementException e) {
            System.err.println("Failed to created socket server");
            e.printStackTrace();
        }

    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        System.out.println("new connection to socket server on port " + port);
        if(!roomJoined) {
            roomSocket = webSocket;
            roomJoined = true;
        }
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        System.out.println("socket server closed");
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        roomSocket.send(s);
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
        e.printStackTrace();
    }

    @Override
    public void onStart() {
        System.out.println("socket server started on port " + port);
        setConnectionLostTimeout(0);
        setConnectionLostTimeout(100);
    }
}
