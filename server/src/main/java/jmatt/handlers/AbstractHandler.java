package jmatt.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public abstract class AbstractHandler implements HttpHandler {
    protected void sendResponse(HttpExchange httpExchange, String response, int responseCode) throws IOException {
        httpExchange.sendResponseHeaders(responseCode, response.getBytes().length);
        OutputStream body = httpExchange.getResponseBody();
        writeString(response, body);
        body.close();
    }

    protected void writeString(String string, OutputStream outputStream) throws IOException {
        OutputStreamWriter streamWriter = new OutputStreamWriter(outputStream);
        streamWriter.write(string);
        streamWriter.flush();
        streamWriter.close();
    }
}
