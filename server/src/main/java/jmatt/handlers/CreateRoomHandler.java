package jmatt.handlers;

import com.sun.net.httpserver.HttpExchange;
import jmatt.services.CreateRoomService;

import java.io.IOException;
import java.net.HttpURLConnection;

public class CreateRoomHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        if(httpExchange.getRequestMethod().toUpperCase().equals("GET")) {

            System.out.println("Received new create room request");

            String roomId = CreateRoomService.createNewRoom();

            if(roomId == null) {
                System.out.println("Failed to create new room");
                sendResponse(httpExchange, "", HttpURLConnection.HTTP_INTERNAL_ERROR);
            } else {
                System.out.println("Successfully created a new room with id " + roomId);
                sendResponse(httpExchange, roomId, HttpURLConnection.HTTP_OK);
            }
        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            httpExchange.getResponseBody().close();
        }
    }
}
