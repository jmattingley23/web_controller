package jmatt.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class WebHandler implements HttpHandler {
    private final String WEB_ROOT = "./web/";

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        if(httpExchange.getRequestMethod().toUpperCase().equals("GET")) {

            //get a string of the URL and remove the leading '/'
            String requestedFilePathStr = httpExchange.getRequestURI().toString().substring(1);
            String filePathToCopy;
            Path fileToCopy;

            if(requestedFilePathStr.equals("")) {
                filePathToCopy = WEB_ROOT + "index.html";
                fileToCopy = FileSystems.getDefault().getPath(filePathToCopy);
                byte[] fileContent = Files.readAllBytes(fileToCopy);
                httpExchange.sendResponseHeaders(HttpsURLConnection.HTTP_OK, fileContent.length);
                httpExchange.getResponseBody().write(fileContent);
                httpExchange.getResponseBody().close();

            } else if(new File(WEB_ROOT + requestedFilePathStr).isFile()) {
                filePathToCopy = WEB_ROOT + requestedFilePathStr; //serve file
                fileToCopy = FileSystems.getDefault().getPath(filePathToCopy);
                byte[] fileContent = Files.readAllBytes(fileToCopy);
                httpExchange.sendResponseHeaders(HttpsURLConnection.HTTP_OK, fileContent.length);
                httpExchange.getResponseBody().write(fileContent);
                httpExchange.getResponseBody().close();

            } else {
                filePathToCopy = WEB_ROOT + "404.html";
                fileToCopy = FileSystems.getDefault().getPath(filePathToCopy);
                byte[] fileContent = Files.readAllBytes(fileToCopy);
                httpExchange.sendResponseHeaders(HttpsURLConnection.HTTP_NOT_FOUND, fileContent.length);
                httpExchange.getResponseBody().write(fileContent);
                httpExchange.getResponseBody().close();
            }
        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            httpExchange.getResponseBody().close();
        }
    }
}
