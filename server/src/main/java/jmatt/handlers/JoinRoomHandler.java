package jmatt.handlers;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import jmatt.data.RoomSingleton;

import java.io.IOException;
import java.net.HttpURLConnection;

public class JoinRoomHandler extends AbstractHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        if(httpExchange.getRequestMethod().toUpperCase().equals("GET")) {
            Headers requestHeaders = httpExchange.getRequestHeaders();
            System.out.println("Received new join room request");

            if(requestHeaders.containsKey("roomId")) {
                String roomId = requestHeaders.getFirst("roomId");
                int socket = RoomSingleton.getInstance().getSocketForRoomId(roomId);

                if(socket == -1) {
                    System.out.println("Failed to find room for given room code");
                    sendResponse(httpExchange, "", HttpURLConnection.HTTP_INTERNAL_ERROR);
                } else {
                    System.out.println("Found room socket " + socket + " for id " + roomId.toUpperCase());
                    sendResponse(httpExchange, String.valueOf(socket), HttpURLConnection.HTTP_OK);
                }
            } else {
                System.out.println("request was missing roomId header");
                sendResponse(httpExchange, "", HttpURLConnection.HTTP_BAD_REQUEST);
            }
        } else {
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            httpExchange.getResponseBody().close();
        }
    }
}
