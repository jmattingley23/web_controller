package jmatt.data;

import java.util.UUID;

public class Room {
    private String roomId;
    private int socket;
    private long lastUsed;

    public Room(int socket) {
        this.socket = socket;
        this.roomId = generateId();
        this.lastUsed = System.currentTimeMillis();
    }

    private String generateId() {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 5).toUpperCase();
    }

    public void useRoom() {
        lastUsed = System.currentTimeMillis();
    }

    public String getRoomId() {
        return roomId;
    }

    public int getSocket() {
        return socket;
    }
}
