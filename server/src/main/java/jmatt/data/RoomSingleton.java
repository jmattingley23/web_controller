package jmatt.data;

import jmatt.sockets.SocketServer;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class RoomSingleton {
    private static volatile RoomSingleton instance = null;
    private List<Room> rooms;

    private RoomSingleton() {
        rooms = new ArrayList<>();
    }

    public static RoomSingleton getInstance() {
        if(instance == null) {
            synchronized (RoomSingleton.class) {
                if(instance == null) {
                    instance = new RoomSingleton();
                }
            }
        }
        return instance;
    }

    public int generateNewSocket() {
        return ThreadLocalRandom.current().nextInt(9000,10001);
    }

    public String createRoom() {

        int socket = generateNewSocket();
        while(isSocketInUse(socket)) {
            socket = generateNewSocket();
        }

        SocketServer socketServer = new SocketServer(socket);
        socketServer.start();

        System.out.println("started new socket server on port " + socket);

        Room room = new Room(socket);
        rooms.add(room);
        System.out.println("There are now " + rooms.size() + " room(s)");
        return room.getRoomId();
    }

    private boolean isSocketInUse(int socket) {
        for(Room room : rooms) {
            if(room.getSocket() == socket) {
                return true;
            }
        }
        return false;
    }

    public int getSocketForRoomId(String roomId) {
        for(Room room : rooms) {
            if(room.getRoomId().equals(roomId.toUpperCase())) {
                return room.getSocket();
            }
        }
        return -1;
    }

    public void useRoom(String roomId) {
        for(Room room : rooms) {
            if(room.getRoomId().equals(roomId.toUpperCase())) {
                room.useRoom();
            }
        }
    }
}
