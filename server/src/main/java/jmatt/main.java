package jmatt;

import com.sun.net.httpserver.*;
import jmatt.handlers.CreateRoomHandler;
import jmatt.handlers.JoinRoomHandler;
import jmatt.handlers.WebHandler;
import jmatt.secure.Keys;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class main {
    private static final short PORT_NUMBER = 443;

    public static void main(String args[]) {
        System.out.print("Initializing HTTPS Server on port " + PORT_NUMBER + "...");
        try {
            InetSocketAddress address = new InetSocketAddress(PORT_NUMBER);

            HttpsServer httpsServer = HttpsServer.create(address, 0);

            KeyStore ks = KeyStore.getInstance("JKS");
            FileInputStream fileInputStream = new FileInputStream("/home/ubuntu/keystore");
            ks.load(fileInputStream, Keys.KEYSTORE_PASSWORD.toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(ks, Keys.KEYSTORE_PASSWORD.toCharArray());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            httpsServer.setHttpsConfigurator(new HttpsConfigurator(sslContext));

            httpsServer.setExecutor(new ThreadPoolExecutor(4, 8, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100)));
            System.out.println("done.");
            System.out.println("Creating contexts...");

            System.out.print("\t/createRoom...");
            httpsServer.createContext("/createRoom", new CreateRoomHandler());
            System.out.println("done.");

            System.out.print("\t/joinRoom...");
            httpsServer.createContext("/joinRoom", new JoinRoomHandler());
            System.out.println("done.");

            System.out.print("\t/...");
            httpsServer.createContext("/", new WebHandler());
            System.out.println("done.");

            System.out.println("Contexts created.");
            System.out.println("Starting...");
            httpsServer.start();
        } catch (IOException | NoSuchAlgorithmException | KeyStoreException | CertificateException | UnrecoverableKeyException | KeyManagementException e) {
            e.printStackTrace();
        }
    }
}
