package jmatt;

import java.net.URISyntaxException;

public class main {

    public static void main(String args[]) {
        System.out.println("Connecting to central server to create room...");
        String roomId = ServerCommunicator.createRoom();
        if(roomId == null) {
            System.err.println("failed to create room on server");
            return;
        }
        System.out.println("Room created successfully.");
        System.out.println("Acquiring socket server port...");
        int port = ServerCommunicator.joinRoom(roomId);
        if(port == -1) {
            System.err.println("failed to get port");
            return;
        }
        System.out.println("Port acquired successfully.");
        System.out.println("Joining socket server as hub...");
        SocketClient socketClient;
        try {
            socketClient = new SocketClient(port);
        } catch (URISyntaxException e) {
            System.err.println("failed to format socket server URI");
            return;
        }
        socketClient.connect();
        System.out.println("Room joined successfully, ready for players.");
        System.out.println("Room ID = " + roomId);
    }
}
