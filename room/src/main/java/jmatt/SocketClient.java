package jmatt;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class SocketClient extends WebSocketClient {
    private static final String SOCKET_SERVER = "wss://jmatt.xyz";

    public SocketClient(int port) throws URISyntaxException {
        super(new URI(SOCKET_SERVER + ":" + port));
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        System.out.println("socket opened");
    }

    @Override
    public void onMessage(String s) {
        System.out.println(s);
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        System.out.println("Disconnected from central server.");
    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();
    }
}
