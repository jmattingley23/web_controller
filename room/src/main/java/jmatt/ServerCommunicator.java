package jmatt;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class ServerCommunicator {

    private static final String SERVER_HOSTNAME = "www.jmatt.xyz";

    public static String createRoom() {
        try {
            URL url = new URL("https://" + SERVER_HOSTNAME + "/createRoom");

            HttpsURLConnection http = (HttpsURLConnection)url.openConnection();
            http.setConnectTimeout(5000);

            http.setDoOutput(false);
            http.setRequestMethod("GET");
            http.connect();

            InputStream responseBody = http.getInputStream();
            String responseData = readString(responseBody);
            return responseData;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int joinRoom(String roomId) {
        try {
            URL url = new URL("https://" + SERVER_HOSTNAME + "/joinRoom");
            HttpsURLConnection http = (HttpsURLConnection)url.openConnection();
            http.setConnectTimeout(5000);

            http.setDoOutput(false);
            http.setRequestMethod("GET");
            http.addRequestProperty("roomId", roomId);
            http.connect();

            InputStream responseBody = http.getInputStream();
            String responseData = readString(responseBody);
            return Integer.parseInt(responseData);
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private static String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }
}
